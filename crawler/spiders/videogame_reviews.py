import scrapy


class DescriptionSpider(scrapy.Spider):
    name = "reviews"
    start_urls = [
        'https://www.metacritic.com/browse/games/release-date/new-releases/all',
    ]

    def parse(self, response):
        for link in response.css('div.clamp-score-wrap a'):
            yield response.follow(link, self.parse_info)

    def parse_info(self, response):
        for link in response.css('#main.col.main_col'):
            yield {
                'Game': link.css('div.product_title h1::text').get(),
                'Score': link.css('div.review_grade div.metascore_w.medium.game.positive.indiv::text').extract()[1].strip(),
                'Description': link.css('div.review_body::text').getall(),
                'Review_Links': link.css('div.source a::attr(href)').getall(),
            }






