import scrapy
import re


class DescriptionSpider(scrapy.Spider):
    name = "metacritic_alltime"
    metacritic_baseurl = 'https://www.metacritic.com/browse/games/score/metascore/all/all/all?sort=desc'

    start_urls = [
        metacritic_baseurl,
    ]

    def parse(self, response):
        for gameinfo in response.css('ol.list_products.list_product_condensed'):
            # title = gameinfo.css('div.product_wrap a::text')
            # score = gameinfo.css('div.metascore_w.small.game.positive::text')
            for i in range(0, 100):
                title = gameinfo.css('div.product_wrap a::text')[i].get().replace("\n", "").strip().replace("  ", "")
                title = re.sub(r'\(.*\)', '', title)
                console = gameinfo.css('div.product_wrap a::text')[i].get().replace("\n", "").strip().replace("  ", "")
                console = re.sub(r'^[^_]*\(', '', console).replace(")", "")
                yield {
                    'Title': title,
                    'Console': console,
                    'Score': gameinfo.css('div.metascore_w.small.game.positive::text')[i].get(),
                }
        for a in response.css('span.flipper.next a'):
            yield response.follow(a, callback=self.parse)








